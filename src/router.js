import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('@/views/Home.vue'),
    },
    {
      path: '/arrival',
      name: 'arrival',
      component: () => import('@/views/Arrival.vue'),
    },
    {
      path: '/departure',
      name: 'departure',
      component: () => import('@/views/Departure.vue'),
    },
  ],
});
