/* eslint-disable no-param-reassign */
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    selectedAirport: {
      name: localStorage.getItem('selectedAirportName') || '',
      code: localStorage.getItem('selectedAirportCode') || '',
    },
  },
  actions: {
  },
  mutations: {
    selectAirport(state, airport) {
      state.selectedAirport.name = airport.name;
      state.selectedAirport.code = airport.value;
      localStorage.setItem('selectedAirportName', airport.name);
      localStorage.setItem('selectedAirportCode', airport.value);
    },
  },
});
