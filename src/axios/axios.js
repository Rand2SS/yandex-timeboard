import axios from 'axios';

const API_URL = 'http://aviation-edge.com/v2/public';
const API_KEY = 'f0f8b3-b7fcd6';

export default axios.create({
  baseURL: `${API_URL}`,
  params: {
    key: API_KEY,
  },
});
